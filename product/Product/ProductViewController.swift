//
//  ProductViewController.swift
//  product
//
//  Created by Sambit Das on 15/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    
  var titleName = ["DigiPOS","Expense Management","DigiPay","DigiPlay","SCM"]
  var subTitleName = ["Point of Sale","Manage Expense","Payment Gateway","Play Music","Supply Chain Management"]
  var logo = ["calendar.circle.fill","dollarsign.square.fill","dollarsign.circle.fill","tv.music.note.fill","tray.2.fill"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return titleName.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProductTableCell
        cell.cellTitle.text = titleName[indexPath.row]
        cell.cellSubtitle.text = subTitleName[indexPath.row]
        cell.cellImage.image = UIImage(systemName: logo[indexPath.row])
        
        return cell
       }

}
