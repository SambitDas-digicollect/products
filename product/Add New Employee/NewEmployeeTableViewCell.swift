//
//  NewEmployeeTableViewCell.swift
//  product
//
//  Created by Sambit Das on 20/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class NewEmployeeTableViewCell: UITableViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var employeeProfile: UIImageView!
    @IBOutlet weak var employeeName: UILabel!
    @IBOutlet weak var employeeMail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
