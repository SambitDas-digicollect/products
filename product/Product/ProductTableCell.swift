//
//  ProductTableCell.swift
//  product
//
//  Created by Sambit Das on 15/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class ProductTableCell: UITableViewCell,UIAlertViewDelegate {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellSubtitle: UILabel!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var subscribeButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        subscribeButton.layer.cornerRadius = 10
        cellView.layer.cornerRadius = 10
        cellImage.tintColor = .white
        self.cellTitle.adjustsFontSizeToFitWidth = true
        self.cellSubtitle.adjustsFontSizeToFitWidth = true
        
        self.cellView.layer.shadowPath =
              UIBezierPath(roundedRect: self.cellView.bounds,
              cornerRadius: self.cellView.layer.cornerRadius).cgPath
        self.cellView.layer.shadowColor = UIColor.lightGray.cgColor
        self.cellView.layer.shadowOpacity = 0.5
        self.cellView.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.cellView.layer.shadowRadius = 1
        self.cellView.layer.masksToBounds = false
        
        
    }
    @IBAction func notificationButtonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Alert", message:
                           "message", preferredStyle: .alert)
                       alertController.addAction(UIAlertAction(title: "cancel", style: .default))

        UIApplication.shared.keyWindow?.rootViewController!.present(alertController, animated: true, completion: nil)

        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

