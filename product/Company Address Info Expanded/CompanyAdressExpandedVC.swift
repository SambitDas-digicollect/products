//
//  CompanyAdressExpandedVC.swift
//  product
//
//  Created by Sambit Das on 22/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

struct NewBranchData{
    var isExpanded:Bool
    var title: String
    var data: BranchDetails
}
struct BranchDetails{
    let branchCode:String
    let branchName:String
    let branchEmail:String
    let branchWebsite:String
    let branchPhone:String
    let branchAltPhone:String
    let street:String
    let town:String
    let zipcode:String
    let city:String
    let state:String
    let country:String
}
class CompanyAdressExpandedVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var YellowfollowButton: UIButton!
    @IBOutlet weak var informationButton: UIButton!
    @IBOutlet weak var employeeButton: UIButton!
    @IBOutlet weak var productButton: UIButton!
    
  
    var sections = [NewBranchData(isExpanded: true, title: "Branch 1", data: BranchDetails(branchCode: "BBK", branchName: "Bangalore", branchEmail: "DigiCollect@gmail.com", branchWebsite: "digicollect.com", branchPhone: "1234", branchAltPhone: "543121", street: "4th Street", town: "Bangalore", zipcode: "560037", city: "Bangalore", state: "Karnataka", country: "India")),
                    NewBranchData(isExpanded: true, title: "Branch 2", data: BranchDetails(branchCode: "DCUSA", branchName: "NewYork", branchEmail: "digiCollect@gmail.com", branchWebsite: "digicollect.com", branchPhone: "+1.212.683.0400", branchAltPhone: "+1.212.683.0400", street: "Avenue of the Americas", town: "NewYork", zipcode: "10036", city: "NY", state: "NY", country: "USA")),
                    NewBranchData(isExpanded: true, title: "Branch 3", data: BranchDetails(branchCode: "DCSP", branchName: "Singapore", branchEmail: "DigiCollect@gmail.com", branchWebsite: "digicollect.com", branchPhone: "+65.6571.1604", branchAltPhone: "+65.6571.1604", street: "Changi Business Park Central", town: "Hansapoint", zipcode: "486030", city: "Singapore", state: "Singapore", country: "Singapore")),
                    NewBranchData(isExpanded: true, title: "Branch 4", data: BranchDetails(branchCode: "DCTL", branchName: "Thailand", branchEmail: "DigiCollect@gmail.com", branchWebsite: "digicollect.com", branchPhone: "12345678", branchAltPhone: "5431213434", street: "Asok Montri Rd", town: "Watthana", zipcode: "10110", city: "Watthana", state: "Krung Thep", country: "Thailand"))]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        YellowfollowButton.layer.cornerRadius = 10
        informationButton.layer.cornerRadius = 10
        employeeButton.layer.cornerRadius = 10
        productButton.layer.cornerRadius = 10
        segmentController.tintColor = .white
        employeeButton.layer.borderWidth = CGFloat(1)
        employeeButton.layer.borderColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        productButton.layer.borderWidth = CGFloat(1)
        productButton.layer.borderColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        informationButton.layer.borderWidth = CGFloat(1)
        informationButton.layer.borderColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          if sections[section].isExpanded{
              return 2
          }else{
              return 1
          }
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
                  let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! HeaderCell
                  cell.indexpath = indexPath
            cell.BranchLabel.text = sections[indexPath.section].title
            cell.delegate = self
                  return cell
              }else{
                  let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! BodyCell
            cell.assignBranchDetails(branchDetails: sections[indexPath.section].data)
                  return cell
              }

      }
      

}
extension CompanyAdressExpandedVC: ButtonDelegate{
    func ButtonPress(indexPath: IndexPath) {
        let status = sections[indexPath.section].isExpanded
        sections[indexPath.section].isExpanded = !status
        tableView.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
    }
    
}
