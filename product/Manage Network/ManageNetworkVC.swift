//
//  ManageNetworkVC.swift
//  product
//
//  Created by Sambit Das on 16/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class ManageNetworkVC: UIViewController ,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
 
    @IBOutlet weak var networkSegment: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var TotalUser: UILabel!
    
    var connectionList = UserCollection.connectionList()
    var followerList = UserCollection.followerList()
    var followingList = UserCollection.followingList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        networkSegment.layer.borderWidth = 1
        networkSegment.layer.borderColor = UIColor.white.cgColor
        self.networkSegment.selectedSegmentIndex = 0
        

        
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch networkSegment.selectedSegmentIndex {
        case 0:
            return connectionList.count
        case 1:
            return followerList.count
        case 2:
            return followingList.count
        default:
            return 0
        }
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ManageNetworkCell
        
        if self.networkSegment.selectedSegmentIndex ==  0 {
            cell.cellName.text = self.connectionList[indexPath.row].name
            cell.cellEmail.text = self.connectionList[indexPath.row].email
            cell.cellImage.image = UIImage(named: self.connectionList[indexPath.row].imageUrl)
            cell.DisconnectButton.setTitle("  Disconnect  ", for: .normal)
            TotalUser.text = "\(connectionList.count) Connections"
            cell.DisconnectButton.isHidden = false
            cell.ThreeDotButton.isHidden = false
            
        }else if self.networkSegment.selectedSegmentIndex ==  1{
            cell.cellName.text = self.followerList[indexPath.row].name
            cell.cellEmail.text = self.followerList[indexPath.row].email
            cell.cellImage.image = UIImage(named: self.followerList[indexPath.row].imageUrl)
            TotalUser.text = "\(followerList.count) followers"
            cell.DisconnectButton.isHidden = true
            cell.ThreeDotButton.isHidden = true
        }else{
            cell.cellName.text = self.followingList[indexPath.row].name
            cell.cellEmail.text = self.followingList[indexPath.row].email
            cell.cellImage.image = UIImage(named: self.followingList[indexPath.row].imageUrl)
            cell.DisconnectButton.setTitle("  Unfollow  ", for: .normal)
            TotalUser.text = "\(followingList.count) Following"
            cell.DisconnectButton.isHidden = false
            cell.ThreeDotButton.isHidden = false
        }
        
        if(UIDevice.current.userInterfaceIdiom == .phone){
            
            cell.cellView.backgroundColor = .systemBlue
            }else{
            cell.cellView.backgroundColor = #colorLiteral(red: 0.1660584457, green: 0.3286434256, blue: 1, alpha: 1)
            }
      
        cell.DisconnectButton.layer.cornerRadius = 10
        cell.DisconnectButton.isHidden = true
        cell.cellView.layer.cornerRadius = 10
        cell.cellImage.layer.cornerRadius = cell.cellImage.frame.height / 2
        cell.cellImage.clipsToBounds = true
    
        return cell
     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(UIDevice.current.userInterfaceIdiom == .phone){
            
            if(UIInterfaceOrientation.portrait.isPortrait){

                return CGSize(width: collectionView.frame.width - 20 , height: 100)
        
            }else{

                return CGSize(width: collectionView.frame.width - 20 , height: 100)
            }
            
        }else{
            if(UIInterfaceOrientation.portrait.isPortrait){

                return CGSize(width: collectionView.frame.width / 2 - 10 , height: 100)
                
            }else{

                return CGSize(width: collectionView.frame.height / 2 - 10 , height: 100)
            }
        }
    
        
        
        
    }
    
    
    @IBAction func networkSegmentPressed(_ sender: Any) {
        
        self.collectionView.reloadData()
        
    }

}



