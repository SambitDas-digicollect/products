//
//  EmployeeData.swift
//  product
//
//  Created by Sambit Das on 20/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import Foundation

struct EmployeeData {
    let name: String
    let email: String
}

class EmployeeDetails {
    static var employeeNameArray : [String] = ["David James","Maxell","Kelly","Daniel","Sara Jhon","John Duo","Lilly Sam","Benny Sam","james","Kavya","Ranjit","Sambit","Debashish"]
     static var employeeEmailArray = ["davidjames@gmail.com","maxwell33@gmail.com","kelly33@gmail.com","daniel34@gmail.com","sarajohn323@gmail.com","john323@gmail.com","lillysam@gmail.com","bennysam32@gmail.com","jamesbond@gmail.com","kavya23@gmail.com","rajit909@gmail.com","sambit650@gmail.com","debashish310@gmail.com"]
 
    class func employeeList() -> [EmployeeData] {
        var list = [EmployeeData]()
        for each in 0..<employeeNameArray.count {
            list.append(EmployeeData(name: employeeNameArray[each], email: employeeEmailArray[each]))
        }
        return list
    }
    
}
