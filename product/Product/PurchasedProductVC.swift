//
//  ViewController.swift
//  product
//
//  Created by Sambit Das on 15/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var cellname = ["Acounting","Cloud","HRMS","IMS","SCM","Contract Management"]
    var logo = ["calendar.circle.fill","icloud.and.arrow.down.fill","person.2.fill","square.stack.3d.up.fill","tray.full.fill","square.and.arrow.up.on.square.fill"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellname.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        
        cell.cellLabel.text = cellname[indexPath.row]
        cell.cellImage.image = UIImage(systemName: logo[indexPath.row])
        cell.cellImage.tintColor = .white
    
        
        return cell
    }
}

