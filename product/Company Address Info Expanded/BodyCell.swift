//
//  BodyCell.swift
//  product
//
//  Created by Sambit Das on 22/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class BodyCell: UITableViewCell {

    @IBOutlet weak var BranchcodeView2: UIView!
    @IBOutlet weak var BranchAddressView2: UIView!
    @IBOutlet weak var BranchAddressBottomView2: UIView!
    
    @IBOutlet weak var branchCodeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var alternateNLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var townLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func assignBranchDetails(branchDetails: BranchDetails){
        branchCodeLabel.text = branchDetails.branchCode
        nameLabel.text = branchDetails.branchName
        emailLabel.text = branchDetails.branchEmail
        websiteLabel.text = branchDetails.branchWebsite
        phoneLabel.text = branchDetails.branchPhone
        alternateNLabel.text = branchDetails.branchAltPhone
        streetLabel.text = branchDetails.street
        townLabel.text = branchDetails.town
        zipLabel.text = branchDetails.zipcode
        cityLabel.text = branchDetails.city
        stateLabel.text = branchDetails.state
        countryLabel.text = branchDetails.country
    }

}
