//
//  OfficeAddressTableviewCell.swift
//  product
//
//  Created by Sambit Das on 21/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class AddressTableviewCell: UITableViewCell {

    @IBOutlet weak var BranchCodeView: UIView!
    @IBOutlet weak var BranchAddressView: UIView!
    @IBOutlet weak var BranchAddressBottomView: UIView!
    @IBOutlet weak var DirectionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        BranchCodeView.layer.cornerRadius = 10
        BranchAddressBottomView.layer.cornerRadius = 10
        DirectionButton.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
