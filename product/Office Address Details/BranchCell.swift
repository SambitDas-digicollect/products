//
//  BranchCell.swift
//  product
//
//  Created by Sambit Das on 21/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

protocol StoreDelegate {
    func didPressButton(indexPath: IndexPath)
}
class BranchCell: UITableViewCell {

    var delegate:StoreDelegate!
    var indexpath: IndexPath!
    @IBOutlet weak var BranchView: UIView!
    @IBOutlet weak var BranchLabel: UILabel!
    @IBOutlet weak var BranchButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func BranchButtonPressed(_ sender: Any) {
        guard let indexPath = indexpath else {return}
        delegate.didPressButton(indexPath: indexPath)
    }
}
