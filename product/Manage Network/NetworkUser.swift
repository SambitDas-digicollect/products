//
//  NetworkUser.swift
//  product
//
//  Created by Sambit Das on 16/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import Foundation
struct NetworkUser {
    let name: String
    let email: String
    let imageUrl: String
}
class UserCollection {
    static var connectionName : [String] = ["David James","Maxell","Kelly","Daniel","Sara Jhon","John Duo","Lilly Sam","Benny Sam","james"]
     static var connectionMail = ["davidjames@gmail.com","maxwell33@gmail.com","kelly33@gmail.com","daniel34@gmail.com","sarajohn323@gmail.com","john323@gmail.com","lillysam@gmail.com","bennysam32@gmail.com","jamesbond@gmail.com"]
    static var connectionProfile = ["userProfile.png","userProfile.png","userProfile.png","userProfile.png","userProfile.png","userProfile.png","userProfile.png","userProfile.png","userProfile.png"]
    
    static var followerName = ["Daniel","Sara Jhon","John Duo"]
    static var followerMail = ["daniel34@gmail.com","sarajohn323@gmail.com","john323@gmail.com"]
    static var followerProfile = ["userProfile.png","userProfile.png","userProfile.png"]
    
    static var followingName = ["John Duo","Lilly Sam","Benny Sam","james"]
    static var followingMail = ["john323@gmail.com","lillysam@gmail.com","bennysam32@gmail.com","jamesbond@gmail.com"]
    static var followingProfile = ["userProfile.png","userProfile.png","userProfile.png","userProfile.png"]
    
    
    class func connectionList() -> [NetworkUser] {
        var list = [NetworkUser]()
        for each in 0..<connectionName.count {
            list.append(NetworkUser(name: connectionName[each], email: connectionMail[each], imageUrl: connectionProfile[each]))
        }
        return list
    }
    class func followerList() -> [NetworkUser] {
        var list = [NetworkUser]()
        for each in 0..<followerName.count {
            list.append(NetworkUser(name: followerName[each], email: followerMail[each], imageUrl: followerProfile[each]))
        }
        return list
    }
    class func followingList() -> [NetworkUser] {
        var list = [NetworkUser]()
        for each in 0..<followingName.count {
            list.append(NetworkUser(name: followingName[each], email: followingMail[each], imageUrl: followingProfile[each]))
        }
        return list
    }
}
