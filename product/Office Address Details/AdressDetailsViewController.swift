//
//  AdressDetailsViewController.swift
//  product
//
//  Created by Sambit Das on 21/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

struct BranchData{
    var isExpanded:Bool
    var title: String
    var data: String
}

class AdressDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
   
  
    @IBOutlet weak var tableView: UITableView!
    var sections = [BranchData(isExpanded: true, title: "Branch 1", data: "data"),
    BranchData(isExpanded: true, title: "Branch 1", data: "data"),
    BranchData(isExpanded: true, title: "Branch 1", data: "data"),
    BranchData(isExpanded: true, title: "Branch 1", data: "data")]
    var headerWidth: CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if sections[section].isExpanded{
                return 2
            }else{
                return 1
            }

       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! BranchCell
            cell.delegate = self
            cell.indexpath = indexPath
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddressTableviewCell
    
            return cell
        }
       }
    @objc private func hideCell(_ sender: UIButton) {
        
    }
}

class underLineStackView : UIStackView{
        
    override func  awakeFromNib() {
        super.awakeFromNib()
        
        //self.borderStyle = .none
        let btmBorder = UIView(frame: .zero)
                          btmBorder.backgroundColor = .white
                          self.addSubview(btmBorder)
                   btmBorder.translatesAutoresizingMaskIntoConstraints = false
                          btmBorder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
                          btmBorder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
                          btmBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
                          btmBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 2).isActive = true
    
                  self.layer.masksToBounds = true
        
    }

}
extension AdressDetailsViewController: StoreDelegate{
    func didPressButton(indexPath: IndexPath) {
        let status = sections[indexPath.section].isExpanded
        sections[indexPath.section].isExpanded = !status
        tableView.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
    }
    
}
