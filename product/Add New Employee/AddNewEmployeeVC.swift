//
//  AddNewEmployeeVC.swift
//  product
//
//  Created by Sambit Das on 20/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class AddNewEmployeeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var backGroundLabel: UILabel!
    @IBOutlet weak var backGroundButton: UIButton!
    
    var employeeList = EmployeeDetails.employeeList()
    var searchData = [EmployeeData]()
    //var searchEmail = [String]()
    var searching : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.delegate = self
        self.tableView.backgroundView = self.backGroundView
        self.tableView.backgroundView?.isHidden = true
        self.backGroundButton.setTitle("Click here to add Employee", for: .normal)
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return searchData.count
        }else{
            return employeeList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewEmployeeTableViewCell
        if searching{
            cell.employeeName.text = searchData[indexPath.row].name
            cell.employeeMail.text = searchData[indexPath.row].email
            
        }else{
            cell.employeeName.text = self.employeeList[indexPath.row].name
            cell.employeeMail.text = self.employeeList[indexPath.row].email
            
        }
        
        cell.employeeProfile.layer.cornerRadius = cell.employeeProfile.frame.height / 2
        return cell
    }
    
    

}
extension AddNewEmployeeVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.tableView.backgroundView?.isHidden = true
            self.searchBar.text = ""
            self.searching = false
            self.tableView.separatorStyle = .singleLine
            self.tableView.reloadData()
            return
        }
        searchData = employeeList.filter {
            $0.name.lowercased().hasPrefix(searchBar.text?.lowercased() ?? "")
        }
        if searchData.isEmpty {
            self.tableView.backgroundView?.isHidden = false

            self.tableView.separatorStyle = .none
            self.backGroundLabel.text = "Sorry we couldn't find any matches for  '\(searchText)'"
        } else {
            self.tableView.backgroundView?.isHidden = true
        }
        searching = true
        tableView.reloadData()
    }

//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searching = false
//        searchBar.text = ""
//        tableView.backgroundView?.isHidden = true
//        tableView.reloadData()
//    }
}

