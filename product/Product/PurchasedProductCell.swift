//
//  TableViewCell.swift
//  product
//
//  Created by Sambit Das on 15/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var ThreeDotButton: UIButton!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var unsubcribeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        cellView.layer.cornerRadius = 10
        unsubcribeButton.layer.cornerRadius = 10
        unsubcribeButton.isHidden = true
        
        
        self.cellView.layer.shadowPath =
              UIBezierPath(roundedRect: self.cellView.bounds,
              cornerRadius: self.cellView.layer.cornerRadius).cgPath
        self.cellView.layer.shadowColor = UIColor.lightGray.cgColor
        self.cellView.layer.shadowOpacity = 0.5
        self.cellView.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.cellView.layer.shadowRadius = 1
        self.cellView.layer.masksToBounds = false
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    
    }
    @IBAction func DotButtonTapped(_ sender: Any) {
        
        ThreeDotButton.isHidden = true
        unsubcribeButton.isHidden = false
    }
    
}
