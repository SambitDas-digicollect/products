//
//  ManageNetworkCell.swift
//  product
//
//  Created by Sambit Das on 16/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class ManageNetworkCell: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellEmail: UILabel!
    @IBOutlet weak var ThreeDotButton: UIButton!
    @IBOutlet weak var DisconnectButton: UIButton!
    
    
    override func awakeFromNib() {
    super.awakeFromNib()
        
        self.cellView.layer.shadowPath =
              UIBezierPath(roundedRect: self.cellView.bounds,
              cornerRadius: self.cellView.layer.cornerRadius).cgPath
        self.cellView.layer.shadowColor = UIColor.lightGray.cgColor
        self.cellView.layer.shadowOpacity = 0.5
        self.cellView.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.cellView.layer.shadowRadius = 1
        self.cellView.layer.masksToBounds = false
        
        
        
    }
    @IBAction func ThrerDotButtonPressed(_ sender: Any) {
        
        DisconnectButton.isHidden = false
        
    }
    
}
